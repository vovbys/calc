package ru.budgett.calc.data;

import lombok.Data;
import lombok.experimental.Accessors;

import java.math.BigDecimal;
import java.time.LocalDate;

@Data
@Accessors(chain = true)
public class CalcData {
    private LocalDate payDate;
    private int payNumber;
    private Double deptBase;
    private Double percent;
    private Double balance;
}
