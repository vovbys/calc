package ru.budgett.calc.data;

import lombok.Data;

import java.util.ArrayList;
import java.util.List;

@Data
public class CalcResponse {
    private List<CalcData> dataList;
    private Double monthPay;
    private Double overpayment;
    private Double total;

    public void addItem(CalcData calcData) {
        if (dataList == null) {
            dataList = new ArrayList<>();
        }
        dataList.add(calcData);
    }
}
