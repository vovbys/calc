package ru.budgett.calc.data;

import lombok.Data;
import lombok.experimental.Accessors;

import javax.validation.constraints.NotNull;

@Data
@Accessors(chain = true)
public class CalcRequest {
    @NotNull(message = "countMonth can not be null")
    private Integer countMonth;
    @NotNull(message = "value can not be null")
    private Integer value;
    @NotNull(message = "percent can not be null")
    private Integer percent;
}
