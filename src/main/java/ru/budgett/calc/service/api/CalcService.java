package ru.budgett.calc.service.api;

import ru.budgett.calc.data.CalcRequest;
import ru.budgett.calc.data.CalcResponse;

public interface CalcService {
    /**
     * Получение данные по кредиту
     */
    CalcResponse calc(CalcRequest request);
}
