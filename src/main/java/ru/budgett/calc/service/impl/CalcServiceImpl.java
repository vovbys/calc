package ru.budgett.calc.service.impl;

import org.springframework.stereotype.Component;
import ru.budgett.calc.data.CalcData;
import ru.budgett.calc.data.CalcRequest;
import ru.budgett.calc.data.CalcResponse;
import ru.budgett.calc.service.api.CalcService;

import javax.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;

import static java.time.LocalDate.now;

@Component
public class CalcServiceImpl implements CalcService {

    @Override
    public CalcResponse calc(CalcRequest request) {
        CalcResponse result = new CalcResponse();

        double monthPay = getMonthPay(request);
        double balance = Double.valueOf(request.getValue());
        LocalDate startDate = now();

        for (int i = 1; i < request.getCountMonth() + 1; i++) {
            double percent = balance * request.getPercent() / 12 / 100;
            double deptBase = monthPay - percent;
            balance = balance - deptBase;

            CalcData calcData = new CalcData()
                    .setPayNumber(i)
                    .setBalance(balance)
                    .setDeptBase(deptBase)
                    .setPayDate(startDate.plusMonths(i))
                    .setPercent(percent);

            result.addItem(calcData);
        }
        result.setMonthPay(monthPay);
        double sum = result.getDataList()
                .stream()
                .mapToDouble(CalcData::getPercent)
                .sum();
        result.setOverpayment(sum);
        result.setTotal(sum + Double.valueOf(request.getValue()));

        return result;
    }

    private double getK(CalcRequest request) {
        double i = Double.valueOf(request.getPercent()) / 100.0 / 12.0;
        double n = request.getCountMonth();

        double tmp = Math.pow(1 + i, n);

        return i * tmp / (tmp - 1);
    }

    private double getMonthPay(CalcRequest request) {
        return getK(request) * request.getValue();
    }
}
