package ru.budgett.calc;

import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableAsync;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import static org.springframework.boot.SpringApplication.run;

@SpringBootApplication
@EnableSwagger2
public class CalcApplication {
    public static void main(String[] args) {
        run(CalcApplication.class, args);
    }
}
