package ru.budgett.calc.controllers;

import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import ru.budgett.calc.data.CalcRequest;
import ru.budgett.calc.data.CalcResponse;
import ru.budgett.calc.service.api.CalcService;

import javax.validation.Valid;

@RestController
@RequiredArgsConstructor
public class CalcController {
    private final CalcService service;

    @GetMapping("test")
    public String test() {
        return "Success";
    }

    @GetMapping(value = "calc")
    public CalcResponse updateDevice(@Valid CalcRequest request) {
        return service.calc(request);
    }
}
