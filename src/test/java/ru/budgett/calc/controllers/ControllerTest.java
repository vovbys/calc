package ru.budgett.calc.controllers;

import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.web.context.WebApplicationContext;
import ru.budgett.calc.AbstractTest;

import static org.springframework.http.MediaType.APPLICATION_JSON;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.webAppContextSetup;

public class ControllerTest extends AbstractTest {
    @Autowired
    private WebApplicationContext webApplicationContext;

    private MockMvc mockMvc;

    @Before
    public final void before() {
        if (this.mockMvc == null) {
            mockMvc = webAppContextSetup(webApplicationContext)
                    .alwaysDo(print())
                    .build();
        }
    }

    @Test
    public void testPushUsers() throws Exception {

        mockMvc.perform(get("/calc")
                .contentType(APPLICATION_JSON)
                .param("countMonth", "48")
                .param("percent", "48")
                .param("value", "2000")
        )
                .andExpect(status().isOk())
                .andExpect(content().contentType(APPLICATION_JSON))
        ;
    }
}
